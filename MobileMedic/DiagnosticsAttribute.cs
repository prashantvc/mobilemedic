using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MobileMedic
{
    [AttributeUsage(AttributeTargets.Class)]
    public class DiagnosticsAttribute : Attribute
    {
        public DiagnosticsAttribute()
        {
            UseCustomData = true;
        }

        public bool UseCustomData { get; set; }
        public UIMode Mode { get; set; }
    }
}
