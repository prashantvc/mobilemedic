﻿using Android.App;

namespace MobileMedic
{
    public static class DiagnosticsManager
    {
        public static ReportField[] DefaultReportFields =
        {
            ReportField.ReportID, ReportField.AppVersionCode, ReportField.AppVersionName, ReportField.PackageName,
            ReportField.FilePath, ReportField.PhoneModel, ReportField.Brand, ReportField.Product,
            ReportField.AndroidVersion, ReportField.Build, ReportField.TotalMemSize, ReportField.AvailableMemSize,
            ReportField.IsSilent, ReportField.StackTrace, ReportField.InitialConfiguration,
            ReportField.CrashConfiguration, ReportField.Display, ReportField.UserComment,
            ReportField.UserAppStartDate, ReportField.UserCrashDate, ReportField.DumpsysMeminfo, ReportField.Logcat,
            ReportField.Eventslog, ReportField.Radiolog,
            ReportField.DeviceID, ReportField.InstallationID, ReportField.DeviceFeatures, ReportField.Environment,
            ReportField.SharedPreferences,
            ReportField.SettingsSystem, ReportField.SettingsSecure
        };


        private static DiagnosticsAttribute _settings;
        private static Application _application;

        internal static DiagnosticsAttribute Settings
        {
            get { return _settings; }
        }


    }
}