﻿using System.Collections.Generic;
using Android.App;
using Android.Content;

namespace MobileMedic
{
    public interface IReporter
    {
        void Initialise(Application application);
        void Send(DiagnosticData data);
    }

    public interface IDiagnosticDataProvider
    {
        IDictionary<string, string> GetErrorData(Context context);
    }
}