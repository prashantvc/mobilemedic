﻿using System;
using System.Collections.Generic;
using Android.Content;
using Android.Runtime;
using Java.Lang;

namespace MobileMedic
{
    public class CrashProcessor : IDisposable
    {
        readonly Context _context;
        readonly ReportField[] _reportFields;
        DateTime _appStartDate;
        UIMode _uiMode;
        string _initialConfiguration;
        readonly List<IReporter> _reporters = new List<IReporter>();
        readonly List<IDiagnosticDataProvider> _diagnosticDataProviders = new List<IDiagnosticDataProvider>();
        
        readonly object _reporterLock = new object();
        readonly object _providerLock = new object();

        public CrashProcessor(Context context, ReportField[] reportFields)
        {
            _context = context;
            _reportFields = reportFields;
            _appStartDate = DateTime.Now;
            _uiMode = DiagnosticsManager.Settings.Mode;
            _initialConfiguration = string.Empty; //TODO:Create configuration

            AndroidEnvironment.UnhandledExceptionRaiser += OnUnhandledExceptionRaiser;
        }

        void OnUnhandledExceptionRaiser(object sender, RaiseThrowableEventArgs e)
        {
            ProcessException(Throwable.FromException(e.Exception));
        }

        void ProcessException(Throwable exception)
        {
            //TODO: Add code
        }

        public void AddDiagnosticDataProvider(IDiagnosticDataProvider provider)
        {
            lock (_providerLock)
            {
                if (!_diagnosticDataProviders.Contains(provider))
                {
                    _diagnosticDataProviders.Add(provider);
                }
            }
        }

        public void AddReporter(IReporter reporter)
        {
            lock (_reporterLock)
            {
                if (!_reporters.Contains(reporter))
                {
                    _reporters.Add(reporter);
                }
            }
        }

        public void RemoveReporter(IReporter reporter)
        {
            lock (_reporterLock)
            {
                if (_reporters.Contains(reporter))
                {
                    _reporters.Remove(reporter);
                }
            }
        }

        public void Dispose()
        {
            AndroidEnvironment.UnhandledExceptionRaiser -= OnUnhandledExceptionRaiser;
        }
    }
}