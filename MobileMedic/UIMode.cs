namespace MobileMedic
{
    public enum UIMode
    {
        Background,
        Notification,
        Toast,
        Dialog
    }
}